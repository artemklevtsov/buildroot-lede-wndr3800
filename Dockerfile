FROM ubuntu:17.04

RUN apt-get update &&\
    apt-get install -y subversion g++ zlib1g-dev build-essential git python \
                       libncurses5-dev gawk gettext unzip file libssl-dev wget &&\
    apt-get clean &&\
    useradd -u 1000 lede &&\
    mkdir -p /data &&\
    chown lede:lede /data

ADD build.sh /usr/local/bin/build-lede

VOLUME ["/data"]

WORKDIR /data

USER lede

ENTRYPOINT ["/usr/local/bin/build-lede"]
