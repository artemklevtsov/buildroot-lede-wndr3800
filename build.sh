#!/bin/bash

set -e

unset SED

if [ ! -d lede ]
then
    git clone https://git.lede-project.org/source.git lede
fi

cd lede

git pull

./scripts/feeds update -a
./scripts/feeds install -a

make defconfig

if [ $1 = "config" ]
then
    make menuconfig
    exit
fi

make -j 5

mkdir -p /data/images
cp /data/lede/bin/targets/ar71xx/generic/lede-ar71xx-generic-wndr3800-* /data/images
