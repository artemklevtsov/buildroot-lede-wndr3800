IMG_NAME = lede-buildroot
WD_PATH = /data

image: Dockerfile build.sh
	docker build --pull -t $(IMG_NAME) .

config:
	docker run --rm -ti -v ${PWD}/data:$(WD_PATH) $(IMG_NAME) config

firmware:
	docker run --rm -ti -v ${PWD}/data:$(WD_PATH) $(IMG_NAME)

.PHONY: config data backup image
